﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    private GameObject targetEnemy;
    private GameObject[] enemies;
    public int distance;
    Enemy healthN;
  

	// Use this for initialization
	private void Start () {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
	}
	
	// Update is called once per frame
	private void Update ()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for(int i= 0; i< enemies.Length; i++)
        {
            if (Vector3.Distance(enemies[i].transform.position, this.transform.position)< distance)
            {
                if(targetEnemy== null)
                {
                    targetEnemy = enemies[i];
                    StartCoroutine(Shoot());
                }
                transform.LookAt(targetEnemy.transform)
                    ;
                if (Vector3.Distance(targetEnemy.transform.position, this.transform.position) >= distance)
                {
                    targetEnemy = null;
                    StopAllCoroutines();
                }
            }
        }
	}

    public IEnumerator Shoot()
    {
        while (targetEnemy != null)
        {
            
            healthN = targetEnemy.GetComponent<Enemy>();
            print(healthN.enemyLife);
            healthN.enemyLife -= 5;
            print(healthN.enemyLife);
            yield return new WaitForSeconds(1.0f);

        }
    }

}
