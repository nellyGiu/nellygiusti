﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour {

    [SerializeField] private GameObject machineGunPrefab;
    [SerializeField] private GameObject missilePrefab;
    [SerializeField] private GameObject sniperPrefab;
    [SerializeField] private GameObject holdingWeapon;
    string energyText;
    // Use this for initialization
    public void createWeapon(string weapon)
    {
        if(weapon== "MachineGun"&& float.Parse(GameObject.Find("MoneyText").GetComponent<Text>().text) >= 4)
        {
            holdingWeapon = Instantiate(machineGunPrefab, Input.mousePosition, Quaternion.identity);
        }
        else if (weapon == "Missile" && float.Parse(GameObject.Find("MoneyText").GetComponent<Text>().text) >= 4)
        {
            holdingWeapon = Instantiate(missilePrefab, Input.mousePosition, Quaternion.identity);
        }
        else if (weapon == "Snipper"&& float.Parse(GameObject.Find("MoneyText").GetComponent<Text>().text) >= 4)
        {
            holdingWeapon = Instantiate(sniperPrefab, Input.mousePosition, Quaternion.identity);
        }
        
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
     
        
            energyText = GameObject.Find("MoneyText").GetComponent<Text>().text;
            if (holdingWeapon != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    holdingWeapon.transform.position = hitInfo.point;
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (hitInfo.transform.CompareTag("placement"))
                        {
                            holdingWeapon = null;
                            Destroy(hitInfo.collider);

                            float energyFloat = float.Parse(energyText);
                            energyFloat = energyFloat - 4;
                            string newEnergy = energyFloat.ToString();
                            GameObject.Find("MoneyText").GetComponent<Text>().text = newEnergy;

                        }
                    }
                }
            }
        
		
	}

}
