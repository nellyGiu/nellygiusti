﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine.UI;
using UnityEngine;
using System;

public class WaveEnemy : MonoBehaviour
{
    public enum EnemyTypes
    {
        buggy,
        helicopter,
        hover
    };

    [Serializable]
    public struct Waves
    {
        public EnemyTypes[] wave;
    }

    public Waves[] waves;

    [SerializeField] private GameObject buggyPrefab;
    [SerializeField] private int currentWave = 0;
    [SerializeField] private int currentEnemy = 0;
    [SerializeField] private int timeBetweenWaves = 5;

    public IEnumerator SendWave()
    {
        while (currentWave< waves.Length)
        {
            while (currentEnemy < waves[currentWave].wave.Length)
            {
                switch (waves[currentWave].wave[currentEnemy])
                {
                    case EnemyTypes.buggy:
                        Instantiate(buggyPrefab, transform.position, Quaternion.identity);
                        break;
                }

                yield return new WaitForSeconds(3);
                currentEnemy++;

               
            }
            currentWave++;

            currentEnemy = 0;
            yield return new WaitForSeconds(timeBetweenWaves);
        }

        
        
       
    }
    public void Wave()
    {
        StartCoroutine(SendWave());
    }






}



