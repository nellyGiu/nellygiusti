﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField] private Vector2 mousePos;
    [SerializeField] private float screenWith;
    [SerializeField] private float screenHeigtht;
    [SerializeField] private float cameraMoveSpeed = 100;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MoveCamera();
	}
    void MoveCamera()
    {
        mousePos = Input.mousePosition;
        screenWith = Screen.width;
        screenHeigtht = Screen.height;

        Vector3 cameraPos = transform.position;
        // izquierda
        if (mousePos.x <= (screenWith * .10f ) && cameraPos.z < .10f)
        {
            if (cameraPos.z < -443)
            {
                cameraPos.z += Time.deltaTime * cameraMoveSpeed;
            }
            
        }
        // abajo
        if (mousePos.y < (screenHeigtht * .10f) && cameraPos.x < .10f)
        {
            if (cameraPos.x >= -356)
            {
                cameraPos.x -= Time.deltaTime * cameraMoveSpeed;
            }

        }
        // -- derecha
        if (mousePos.x > (screenWith * .90f) && cameraPos.z < .90f)
        {
            if (cameraPos.z >= -781)
            {
                cameraPos.z -= Time.deltaTime * cameraMoveSpeed;
            }
            
        }
        // -- arriba
        if (mousePos.y > (screenHeigtht * .90f) && cameraPos.x < .90f)
        {
            if (cameraPos.x >= -560)
            {
                cameraPos.x += Time.deltaTime * cameraMoveSpeed;
            }
            
        }

        transform.position = cameraPos;
    }
}
