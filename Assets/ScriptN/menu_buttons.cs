﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu_buttons : MonoBehaviour {

    public Canvas levelsCanvas;
    public Canvas menuCanvas;

	void Start () {
        levelsCanvas.enabled = false;
        menuCanvas.enabled = true;
	}

    public void LevelButton()
    {
        levelsCanvas.enabled = true;
        menuCanvas.enabled = false;
    }
    public void  QuitGame()
    {
        Application.Quit();
    }
    public void Back()
    {
        levelsCanvas.enabled = false;
        menuCanvas.enabled = true;
    }
    public void Level1()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level1_N");
    }
}
