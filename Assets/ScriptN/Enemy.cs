﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    private NavMeshAgent agent;
    private GameObject target;
    public int life=0;
    public int enemyLife;
    string energy;
    string HQhealth;

    // Use this for initialization
    void Start() {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("PlayerBaseTarget");
        agent.SetDestination(target.transform.position);
        enemyLife = 3;
        
	}
	
	// Update is called once per frame
	void Update () {
		if (enemyLife < 1)
        {
            energy = GameObject.Find("energy").GetComponent<Text>().text;
            float energyF = float.Parse(energy);
            energyF += 4;
            string newEnergy = energyF.ToString();
            GameObject.Find("energy").GetComponent<Text>().text = newEnergy;
            Destroy(this.gameObject);
        }
        
        //if((transform.position.x +1 <= target.transform.position.x) && (transform.position.z +1 <= target.transform.position.z)  )
	}
    public void CarLife(int damage)
    {
        life = life - damage;
        if (life == 0)
        {
            Destroy(this.gameObject, 1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "cubo")
        {     
            Destroy(gameObject, 1);
        }
    }

    



}
